<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="assets/Style.css">
    <title>Todo List</title>
</head>
<body>
    <div class="container">
        <div class="title">
            <h2>TodoList</h2>
        </div>
        <div class="add">
            <form action="?action=add" method="post">
                <input type="text" name="title" placeholder="Task Title">
                <input type="submit" value="Add">
            </form>
        </div>

        <div class="todos">

            <?php foreach($todos as $todo): ?>
                <div class="todo<?php  if($todo['done'] == 1){echo " "."done"; } ?>">
                    <p><?php echo $todo['title'] ?></p>
                    <div class="btn">
                        <a href="?<?php if($todo['done'] == 1){echo "done=".$todo['id']; }else{ echo "undo=".$todo['id']; }?>"><button><?php if($todo['done'] == 1){echo "undo"; }else{ echo "done"; }?></button></a>
                        <a href="?delete=<?php echo $todo['id']; ?>"><button name="delete">X</button></a>
                    </div>
                </div>
            <?php endforeach ; ?>




        </div>

    </div>
</body>
</html>