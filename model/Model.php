<?php


class Model {


    public function __construct(private $host,private $dbname,private $user,private $password)
    {
        try{
            $this->connection = new PDO("mysql:host=$this->host;dbname=$this->dbname",$this->user,$this->password);
        }catch(PDOException $e){
            echo 'Connection failde'. $e->getMessage();
        }
    }



    public function ExecuteQuery($sql,$params=[]){
        try{
            $query = $this->connection->prepare($sql);
            $query->execute($params);
            return $query ;

        }catch(PDOException $e){ 
            echo $e->getMessage();
        }

    }
}



