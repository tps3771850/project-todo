<?php

class TodoModel{
    private $db;

    public function __construct($db)
    {
        $this->db=$db;
    }


    public function Add($title)
    {
        $sql = "INSERT INTO todo (title) VALUES (?)";
        $this->db->ExecuteQuery($sql,[$title]);
    
    }

    public function Done($id)
    {
        $sql = "UPDATE todo SET done = 1  WHERE id =?";
        $this->db->ExecuteQuery($sql,[$id]);
    }

    public function Undo($id)
    {
        $sql = "UPDATE todo SET done =0  WHERE id =?";
        $this->db->ExecuteQuery($sql,[$id]);
    }

    public function Delete($id)
    {
        $sql = "DELETE FROM todo WHERE id = ?";
        $this->db->ExecuteQuery($sql , [$id]);

    }


    public function GetAll()
    {
        $sql = "SELECT * FROM todo ORDER BY id DESC" ;
        return $this->db->ExecuteQuery($sql,[])->fetchAll(PDO::FETCH_ASSOC);
    }
}