<?php

class TodoController{
    
    private $db;

    public function __construct($db)
    {
        $this->todo = new TodoModel($db);
    }

    public function TodoList()
    {
        $todos = $this->todo->GetAll();
        include('./view/TodoList.php');

    }


    public function CreateTodo($title)
    {
        $this->todo->Add($title);

    }

    public function Delete($id)
    {
        $this->todo->Delete($id);
    }

    public function Done($id)
    {
        $this->todo->Done($id);
    }

    public function Undo($id)
    {
        $this->todo->Undo($id);
    }

}